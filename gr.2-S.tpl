The Constitution is amended as follows:

---------------------------------------------------------------------------
@DIFF@
---------------------------------------------------------------------------

As a transitional measure, if this GR is passed after January 1st, 2015,
then the provision of section §6.2.7.1 is taken to have occurred on January
1st, 2015.
