PROPOSALS = 2 2-R max 2-S

PUBLISH_URL = people.debian.org:public_html/gr-ctte-term-limit/

DIFFS = $(patsubst %,constitution.%.diff,$(PROPOSALS))
WDIFFS = $(patsubst %,constitution.%.wdiff,$(PROPOSALS))
GRS = $(patsubst %,gr.%.txt,$(PROPOSALS))

all: $(DIFFS) $(WDIFFS) $(GRS)

constitution.%.diff: constitution.txt.orig constitution.%.txt
	diff -u $^ > $@ || true

constitution.%.wdiff: constitution.txt.orig constitution.%.txt
	command -v wdiff > /dev/null
	wdiff $^ > $@ || true

gr.%.txt: gr.%.tpl constitution.%.diff fill
	./fill $* < $< > $@

publish: publish-stamp
publish-stamp: $(GRS)
	rsync -avuz --delete gr.*.txt $(PUBLISH_URL)
	touch $@

clean:
	rm -f constitution.*.diff constitution.*.wdiff gr.*.txt

.PHONY: all clean publish
