The Constitution is amended as follows:

---------------------------------------------------------------------------
@DIFF@
---------------------------------------------------------------------------

As a transitional measure, the terms of any current members of the Technical
Committee that exceed the limit above at the time of adoption of this General
Resolution shall instead expire every 6 months, starting one month after this
General Resolution is passed, in descending order of seniority.
