The Constitution is amended as follows:

---------------------------------------------------------------------------
@DIFF@
---------------------------------------------------------------------------

As a transitional measure, the first automatic expiry of membership of the
Technical Committee will happen on January 1st, 2016.
